package wee.flutter.wee_camera_mlkit.camera

import android.content.Context
import android.os.Build
import androidx.constraintlayout.widget.ConstraintLayout
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.annotation.RequiresApi
import co.metalab.asyncawait.async
import com.google.android.gms.vision.face.Face
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import kotlinx.android.synthetic.main.control_verifyface_layout.view.*
import wee.flutter.wee_camera_mlkit.R
import wee.flutter.wee_camera_mlkit.mlkit.FaceDetectionProcessor
import wee.flutter.wee_camera_mlkit.mlkit.FaceResult
import wee.flutter.wee_camera_mlkit.mlkit.GraphicOverlay
import wee.flutter.wee_camera_mlkit.vm.pFacing
import java.io.IOException


class VerifyFaceControl : ConstraintLayout {

    private val CAMERA_REQUEST_ID = 513469796
    //private var mActivity: Activity? = null
    private var mRootView: View? = null
    var isOn = false
    //---
    private val TAG = "DetectFaceControl"
    private var mCameraSource: CameraSource? = null
    private var mPreviewFullScreen: PreviewFullScreen? = null
    private var mGraphicOverlay: GraphicOverlay? = null
    private var mFaceShape: ImageView?=null
    //---
    private var mFaceDetectionProcessor: FaceDetectionProcessor? = null
    private var mLayoutCameraContainer: ConstraintLayout?=null
    private var mCameraInclude: ConstraintLayout?=null
    private var mLayoutImageContainer: ConstraintLayout?=null
    //---
    private var mCurrentFaceResult: FaceResult?=null
    private var isInside = true
    private var isVerify = false
    private var isTracking = false
    private var currentDetectID = -2
    private var isAddGifFace = false
    //private var mGifFaceControl: GifFaceControl?=null
    //---
    private var failedCount = 4
    //---
    private var mOnVerifyFaceListener: OnVerifyFaceListener?=null
    //---
    /*private val mLoaderCallback = object : BaseLoaderCallback(context) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    Log.e("OpenCV", "OpenCV loaded successfully")
                }
                else -> {
                    super.onManagerConnected(status)
                }
            }
        }
    }*/
    //---
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    //---
    init {
        if (mRootView == null) {
            mRootView = inflate(context, R.layout.control_verifyface_layout, this)
            //-- Camera review
            val cameraContainer = mRootView!!.findViewById<View>(R.id.verifyFaceControl_camera)
            mPreviewFullScreen = cameraContainer.findViewById(R.id.faceTracker_cameraSourcePreview)
            mGraphicOverlay = cameraContainer.findViewById(R.id.faceTracker_graphicOverlay)
            //---
            mLayoutCameraContainer = mRootView!!.findViewById(R.id.verifyFaceControl_cameraContainer)
            mCameraInclude = mRootView!!.findViewById(R.id.verifyFaceControl_camera)
            mFaceShape = mRootView!!.findViewById(R.id.verifyFaceControl_cameraShapeFace)
            //---
        }
    }


    /*fun setActivity(activity: Activity) {
        mActivity = activity
        createCameraSource()
    }*/


    fun createCameraSource() {
        if (mCameraSource == null) {
            mCameraSource = CameraSource(context,mGraphicOverlay!!)
            mCameraSource!!.setFacing(pFacing)
            //mJsonFaceControl = JsonFaceControl(context)
            //mGifFaceControl = GifFaceControl(context)
        }

        try {
            mFaceDetectionProcessor = FaceDetectionProcessor()
            mCameraSource!!.setMachineLearningFrameProcessor(mFaceDetectionProcessor!!)
            mFaceDetectionProcessor!!.setOnVerifyFaceListener(object : FaceDetectionProcessor.VerifyFaceListener{
                override fun onTrackingFace(face: FirebaseVisionFace?) {
                    /*if(!isTracking) {
                        isTracking = true
                        kotlin.runCatching {
                            if (face != null && isOn) {
                                //mCameraSource!!.setMetering(mRootView, face)
                                if (!isAddGifFace) {
                                    //---
                                    if (mGifFaceControl != null) verifyFaceControl_faceJsonOverlay.removeView(mGifFaceControl)
                                    //---
                                    val layoutParams = ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT)
                                    mGifFaceControl!!.layoutParams = layoutParams
                                    verifyFaceControl_faceJsonOverlay.addView(mGifFaceControl)
                                    isAddGifFace = true

                                }

                                if (mGifFaceControl != null) {
                                    verifyFaceControl_faceRegister.visibility = View.INVISIBLE
                                    mOnVerifyFaceListener?.onDetecting(true)
                                    verifyFaceControl_faceJsonOverlay.visibility = View.VISIBLE
                                    //mJsonFaceControl!!.setPlayJson()
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                        mGifFaceControl!!.setFace(face, isInside)
                                    }

                                }


                            } else {
                                if (mGifFaceControl != null) {
                                    //mJsonFaceControl!!.setPauseJson()
                                    mOnVerifyFaceListener?.onDetecting(false)
                                    verifyFaceControl_faceJsonOverlay.visibility = View.INVISIBLE
                                    verifyFaceControl_faceRegister.visibility = View.VISIBLE
                                }
                            }
                        }.onSuccess {
                            isTracking = false
                        }
                    }*/

                    if (face != null && isOn && !isVerify) {
                        //---
                        isVerify = true
                        if(currentDetectID != face.trackingId){
                            //-- Change ID
                            currentDetectID = face.trackingId
                            failedCount = 4
                        }
                        async {
                            var faceResult:FaceResult?=null
                            await {
                                faceResult = mFaceDetectionProcessor?.faceResultRaw
                            }
                            if(faceResult!=null){
                                mOnVerifyFaceListener?.onResult(true,faceResult!!.faceData,face)
                            }
                            isVerify = false
                        }

                    }

                }

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun onUpdateFace(largestFace: FaceResult?) {

                }

            })
        } catch (e: Exception) {
            Log.e("Error create Camera", e.toString())
        }
    }

    /*private fun getBestFace(listFace: ArrayList<FaceResult>):FaceResult{
        var bestFace = FaceResult()
        for(item in listFace){
            if(bestFace.BlurValue < item.BlurValue){
                bestFace = item
            }
        }
        return bestFace
    }*/

    fun startAction(){
        /*if (!OpenCVLoader.initDebug()) {
            Log.e("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization")
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_4_0, context, mLoaderCallback)
        } else {
            Log.e("OpenCV", "OpenCV library found inside package. Using it!")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }*/
        //---
        startCameraSource()
        //---
        //isOn = true
        //---

    }

    private fun startCameraSource() {
        if (mCameraSource != null) {
            try {
                if (mPreviewFullScreen == null) {
                    Log.e(TAG, "resume: Preview is null")
                }
                if (mGraphicOverlay == null) {
                    Log.e(TAG, "resume: graphOverlay is null")
                }

                mPreviewFullScreen?.start(mCameraSource!!, mGraphicOverlay!!)

            } catch (e: IOException) {
                Log.e(TAG, "Unable to start camera source.", e)
                mCameraSource?.release()
                mCameraSource = null
            }
        }

    }


    fun resetData(){
        mCurrentFaceResult = null
        //---
        mLayoutImageContainer!!.visibility = View.GONE
        mLayoutCameraContainer!!.visibility = View.VISIBLE

    }

    fun releaseCamera(){
        if(mCameraSource!=null) {
            mCameraSource!!.release()
            mPreviewFullScreen!!.release()
        }
    }

    fun setVerifyFaceListener(listener: OnVerifyFaceListener){
        mOnVerifyFaceListener = listener
    }

    interface OnVerifyFaceListener{
        fun onDetecting(boolean: Boolean)
        fun onResult(boolean: Boolean, faceByteArray : ByteArray?,face: FirebaseVisionFace?)
    }
}