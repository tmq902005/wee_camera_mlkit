/*
package wee.flutter.wee_camera_mlkit.camera

import android.annotation.TargetApi
import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.util.Log
import android.util.Size
import android.view.View
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import wee.flutter.wee_camera_mlkit.R
import wee.flutter.wee_camera_mlkit.vm.pFacing
import wee.flutter.wee_camera_mlkit.vm.pScaleX
import wee.flutter.wee_camera_mlkit.vm.pScaleY


class GifFaceControl : ConstraintLayout {

    private var mRootView: View?=null
    private var idFace = -1
    private var mGifContainer: ConstraintLayout?=null
    private var isCurrent = true
    var faceAreaListener: FaceAreaListener?=null
    //---
    constructor(context: Context):super(context)
    constructor(context: Context, attrs: AttributeSet):super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int):super(context, attrs, defStyleAttr)


    //---
    init {
        if(mRootView==null) {
            mRootView = inflate(context, R.layout.control_gifface_layout, this)
            mGifContainer = mRootView!!.findViewById(R.id.gifFaceControl_gifContainer)
            */
/*val mediaController = MediaController(context)
            mediaController.setAnchorView(videoFaceControl_videoView)
            videoFaceControl_videoView.setMediaController(mediaController)
            videoFaceControl_videoView.setVideoURI(Uri.parse("android.resource://" + context.packageName + "/" + R.raw.face_video))
            videoFaceControl_videoView.start()
            mediaController.background = resources.getDrawable(android.R.color.transparent)*//*

        }
    }


    fun setIdFace(id: Int){
        idFace = id
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun setFace(face: FirebaseVisionFace, inside: Boolean){
        Log.e("boundingBox","${face.boundingBox}")
        val x = translateX(face.boundingBox.exactCenterX().toFloat())
        val y = translateY(face.boundingBox.exactCenterY().toFloat())
        Log.e("ExactCenter","Center ${face.boundingBox.exactCenterX()}-${face.boundingBox.exactCenterY()}")
        setIdFace(face.trackingId)
        val xOffset = scaleX(face.boundingBox.width().toFloat())
        val yOffset = scaleY(face.boundingBox.height().toFloat())
        val left = x - xOffset
        val top = y - yOffset
        val right = x + xOffset
        val bottom = y + yOffset
        mGifContainer!!.x = left
        mGifContainer!!.y = (top + (face.boundingBox.height() - face.boundingBox.width())* pScaleY)
        val w = face.boundingBox.width()*2f* pScaleX
        val h = face.boundingBox.height()*1.8f * pScaleY
        val size = Size(w.toInt(),h.toInt())
        //Log.e("gifFaceControl_gif","Size: $size - ${gifFaceControl_gif.x} - ${gifFaceControl_gif.y}")

        val params = mGifContainer!!.layoutParams
        params.width =  size.width
        params.height = size.height
        val padding = size.width*0.2f
        mGifContainer!!.setPadding(padding.toInt(),padding.toInt(),padding.toInt(),padding.toInt())
        mGifContainer!!.layoutParams = params


    }
    fun scaleX(horizontal: Float): Float {
        return horizontal * 1.0f * pScaleX
    }

    fun scaleY(vertical: Float): Float {
        return vertical * 1.0f * pScaleY
    }

    private fun translateX(x: Float): Float {
        return if(pFacing== CameraSource.CAMERA_FACING_FRONT){
            mRootView!!.width - scaleX(x)
        }else{
            scaleX(x)
        }
    }

    */
/**
     * Adjusts the y coordinate from the preview's coordinate system to the view coordinate system.
     *//*

    private fun translateY(y: Float): Float {
        return scaleY(y)
    }
    private fun calculateTapArea(v: View, oldx: Float, oldy: Float, coefficient: Float): Rect {

        val y = v.height - oldx

        val focusAreaSize = 50f

        val areaSize = java.lang.Float.valueOf(focusAreaSize * coefficient)!!.toInt()
        val centerX = (oldy / v.width * 2000 - 1000).toInt()
        val centerY = (y / v.height * 2000 - 1000).toInt()

        val left = clamp(centerX - areaSize / 2, -1000, 1000)
        val right = clamp(left + areaSize, -1000, 1000)
        val top = clamp(centerY - areaSize / 2, -1000, 1000)
        val bottom = clamp(top + areaSize, -1000, 1000)

        return Rect(left, top, right, bottom)
    }
    private fun clamp(x: Int, min: Int, max: Int): Int {
        if (x > max) {
            return max
        }
        return if (x < min) {
            min
        } else x
    }


    interface FaceAreaListener{
        fun areaFace(rect : Rect)
    }

}*/
