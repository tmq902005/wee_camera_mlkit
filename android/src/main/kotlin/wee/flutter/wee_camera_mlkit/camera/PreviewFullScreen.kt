// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package wee.flutter.wee_camera_mlkit.camera

import android.content.Context
import android.content.res.Configuration
import android.graphics.SurfaceTexture
import android.util.AttributeSet
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.TextureView
import android.view.ViewGroup

import wee.flutter.wee_camera_mlkit.Utils
import wee.flutter.wee_camera_mlkit.mlkit.GraphicOverlay
import wee.flutter.wee_camera_mlkit.vm.pCameraHeight
import wee.flutter.wee_camera_mlkit.vm.pCameraWidth
import wee.flutter.wee_camera_mlkit.vm.pScaleX
import wee.flutter.wee_camera_mlkit.vm.pScaleY

import java.io.IOException

/** Preview the camera image in the screen.  */
class PreviewFullScreen(//PREVIEW VISUALIZERS FOR BOTH CAMERA1 AND CAMERA2 API.
        private val mContext: Context, attrs: AttributeSet) : ViewGroup(mContext, attrs) {
    private val mSurfaceView: SurfaceView
    private var mStartRequested: Boolean = false
    private var mSurfaceAvailable: Boolean = false
    private var mCameraSource: CameraSource? = null
    private var usingCameraOne: Boolean = false
    private var mOverlay: GraphicOverlay? = null
    private val viewAdded = false
    private val screenWidth: Int
    private val screenHeight: Int
    private val screenRotation: Int


    private val mSurfaceTextureListener = object : TextureView.SurfaceTextureListener {
        override fun onSurfaceTextureAvailable(texture: SurfaceTexture, width: Int, height: Int) {
            mSurfaceAvailable = true
            mOverlay!!.bringToFront()
            try {
                startIfReady()
            } catch (e: IOException) {
                Log.e(TAG, "Could not start camera source.", e)
            }

        }

        override fun onSurfaceTextureSizeChanged(texture: SurfaceTexture, width: Int, height: Int) {}
        override fun onSurfaceTextureDestroyed(texture: SurfaceTexture): Boolean {
            mSurfaceAvailable = false
            return true
        }

        override fun onSurfaceTextureUpdated(texture: SurfaceTexture) {}
    }

    /*@Override
  protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
    boolean isPortrait  = isPortraitMode();
    screenHeight = right;
    screenWidth = bottom;
    if (isPortrait) {
      int tmpScr = screenWidth;
      screenWidth = screenHeight;
      screenHeight = tmpScr;
    }
    int ratioWidth = PublicKt.getPCameraWidth();
    int ratioHeight = PublicKt.getPCameraHeight();
    if (isPortrait) {
      int tmp = ratioWidth;
      ratioWidth = ratioHeight;
      ratioHeight = tmp;
    }
    float scaleX = (float)screenWidth/(float)ratioWidth;
    float scaleY = (float)screenHeight/(float)ratioHeight;
    PublicKt.setPScaleX(scaleX);
    PublicKt.setPScaleY(scaleY);

    childHeight = screenHeight;
    childWidth = (int) (((float) screenHeight / (float) ratioHeight) * ratioWidth);
    //---
    if(childWidth> screenWidth){
      childLeft=(childWidth-right+left)/2;
      childTop=(childHeight-bottom+top)/2;
    }
    else if(childWidth< screenWidth){
      childWidth = screenWidth;
      childHeight = (int) (((float) screenWidth / (float) ratioWidth) * ratioHeight);
      if (childHeight > screenHeight) {
        childTop=(childHeight- screenHeight);
      }
    }

    for (int i = 0; i < getChildCount(); ++i) {
      getChildAt(i).layout(-childLeft, -childTop, childWidth-childLeft, childHeight);
    }

    try {
      startIfReady();
    } catch (IOException e) {
      Log.e(TAG, "Could not start camera source.", e);
    }

  }*/

    private val isPortraitMode: Boolean
        get() {
            val orientation = context.resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                return false
            }
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                return true
            }

            Log.d(TAG, "isPortraitMode returning false by default")
            return false
        }

    init {
        mStartRequested = false
        mSurfaceAvailable = false

        mSurfaceView = SurfaceView(mContext)
        mSurfaceView.holder.addCallback(SurfaceCallback())
        screenHeight = Utils.getScreenHeight(mContext)
        screenWidth = Utils.getScreenWidth(mContext)
        screenRotation = Utils.getScreenRotation(mContext)
        mStartRequested = false
        mSurfaceAvailable = false
        addView(mSurfaceView)
    }

    @Throws(IOException::class)
    fun start(cameraSource: CameraSource?) {
        if (cameraSource == null) {
            stop()
        }

        mCameraSource = cameraSource

        if (mCameraSource != null) {
            mStartRequested = true
            startIfReady()
        }
    }


    @Throws(IOException::class)
    fun start(cameraSource: CameraSource, overlay: GraphicOverlay) {
        usingCameraOne = true
        mOverlay = overlay
        start(cameraSource)
    }


    fun stop() {
        if (mCameraSource != null && mStartRequested) {
            mCameraSource!!.stop()
        }
    }

    fun pause() {
        if (mCameraSource != null && mStartRequested) {
            mCameraSource!!.pauseCamera()
        }
    }

    fun release() {
        if (mCameraSource != null && mStartRequested) {
            mCameraSource!!.release()
            mCameraSource = null
        }
    }

    @Throws(IOException::class)
    private fun startIfReady() {
        if (mStartRequested && mSurfaceAvailable) {
            try {
                mCameraSource!!.start(mSurfaceView.holder)
                if (mOverlay != null) {
                    val size = mCameraSource!!.previewSize
                    if (size != null) {
                        val min = Math.min(size.width, size.height)
                        val max = Math.max(size.width, size.height)
                        Log.e("Camera", "Camera Review Size " + size.width + "x" + size.height)
                        // FOR GRAPHIC OVERLAY, THE PREVIEW SIZE WAS REDUCED TO QUARTER
                        // IN ORDER TO PREVENT CPU OVERLOAD
                        mOverlay!!.setCameraInfo(size.width, size.height, mCameraSource!!.cameraFacing)
                        mOverlay!!.clear()
                    } else {
                        stop()
                    }
                }
                mStartRequested = false
            } catch (e: SecurityException) {
                Log.d(TAG, "SECURITY EXCEPTION: $e")
            }

        }
    }

    private inner class SurfaceCallback : SurfaceHolder.Callback {
        override fun surfaceCreated(surface: SurfaceHolder) {
            mSurfaceAvailable = true
            try {
                startIfReady()
            } catch (e: IOException) {
                Log.e(TAG, "Could not start camera source.", e)
            }

        }

        override fun surfaceDestroyed(surface: SurfaceHolder) {
            mSurfaceAvailable = false
        }

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {}
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        var previewWidth = pCameraWidth
        var previewHeight = pCameraHeight
        if (mCameraSource != null) {
            val size = mCameraSource!!.previewSize
            if (size != null) {
                previewWidth = size.width
                previewHeight = size.height
            }
        }

        // Swap width and height sizes when in portrait, since it will be rotated 90 degrees
        if (isPortraitMode) {
            val tmp = previewWidth
            previewWidth = previewHeight
            previewHeight = tmp
        }

        val viewWidth = right - left
        val viewHeight = bottom - top

        val childWidth: Int
        val childHeight: Int
        var childXOffset = 0
        var childYOffset = 0
        val widthRatio = viewWidth.toFloat() / previewWidth.toFloat()
        val heightRatio = viewHeight.toFloat() / previewHeight.toFloat()

        /*val scaleX = previewWidth.toFloat() / widthRatio
        val scaleY = previewHeight.toFloat() / heightRatio*/

        pScaleX = widthRatio
        pScaleY = heightRatio

        // To fill the view with the camera preview, while also preserving the correct aspect ratio,
        // it is usually necessary to slightly oversize the child and to crop off portions along one
        // of the dimensions.  We scale up based on the dimension requiring the most correction, and
        // compute a crop offset for the other dimension.
        if (widthRatio > heightRatio) {
            childWidth = viewWidth
            childHeight = (previewHeight.toFloat() * widthRatio).toInt()
            childYOffset = (childHeight - viewHeight) / 2
        } else {
            childWidth = (previewWidth.toFloat() * heightRatio).toInt()
            childHeight = viewHeight
            childXOffset = (childWidth - viewWidth) / 2
        }

        for (i in 0 until childCount) {
            // One dimension will be cropped.  We shift child over or up by this offset and adjust
            // the size to maintain the proper aspect ratio.
            getChildAt(i).layout(
                    -1 * childXOffset, -1 * childYOffset,
                    childWidth - childXOffset, childHeight - childYOffset)
        }

        try {
            startIfReady()
        } catch (e: IOException) {
            Log.e(TAG, "Could not start camera source.", e)
        }

    }

    companion object {
        var childWidth = 0
        var childHeight = 0
        var childTop = 0
        var childLeft = 0
        private val TAG = "CameraSourcePreview"
    }
}
