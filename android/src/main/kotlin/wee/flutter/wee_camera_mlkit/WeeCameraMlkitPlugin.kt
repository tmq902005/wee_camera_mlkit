package wee.flutter.wee_camera_mlkit

import io.flutter.plugin.common.PluginRegistry.Registrar


class WeeCameraMlkitPlugin {
  companion object {
    @JvmStatic
    fun registerWith(registrar: Registrar) {
        registrar.platformViewRegistry()
                .registerViewFactory("wee.flutter.weecameramlkit/camera", WeeCameraMlkitFactory(registrar.messenger()))
    }

  }

}


