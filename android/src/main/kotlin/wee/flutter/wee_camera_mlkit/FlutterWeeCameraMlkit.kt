package wee.flutter.wee_camera_mlkit

import android.Manifest
import android.content.Context
import android.util.Log
import android.view.View
import com.gun0912.tedpermission.PermissionListener
import io.flutter.plugin.common.BinaryMessenger
import io.flutter.plugin.common.EventChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.platform.PlatformView
import wee.flutter.wee_camera_mlkit.camera.VerifyFaceControl
import java.util.*
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.gun0912.tedpermission.TedPermission


class FlutterWeeCameraMlkit constructor(context: Context, messenger: BinaryMessenger, id:Int): PlatformView, MethodChannel.MethodCallHandler {
    //constructor(registrar: PluginRegistry.Registrar,context: Context, messenger: BinaryMessenger, id:Int):this(context,messenger,id)

    private val mVerifyFaceControl = VerifyFaceControl(context)
    private val mMethodChannel = MethodChannel(messenger, "wee.flutter.weecameramlkit/camera_$id")
    private var channelStream = EventChannel(messenger,"wee_camera_mlkit/stream")
    private var streamHandler: EventChannel.StreamHandler?=null
    private var mResult: MethodChannel.Result?=null
    private var mEventSink: EventChannel.EventSink?=null
    private val mContext = context
    private val mMessenger = messenger

    init {
        mMethodChannel.setMethodCallHandler(this)
        FirebaseApp.initializeApp(context)
    }


    override fun getView(): View {
        return mVerifyFaceControl
    }

    override fun dispose() {
        mVerifyFaceControl.releaseCamera()
    }

    override fun onMethodCall(call: MethodCall?, result: MethodChannel.Result?) {
        this.mResult = result

        when(call!!.method){

            "startCamera" ->{
                checkPermission()
            }

            "startFaceStream" ->{
                mVerifyFaceControl.isOn = true
                streamHandler = object : EventChannel.StreamHandler{
                    override fun onListen(p0: Any?, eventSink: EventChannel.EventSink?) {
                        mEventSink = eventSink
                    }

                    override fun onCancel(p0: Any?) {

                    }

                }
                channelStream.setStreamHandler(streamHandler)
                mResult!!.success(true)
            }
            "stopFaceStream" ->{
                mEventSink = null
                mVerifyFaceControl.isOn = false
            }
            else ->{
                mResult?.notImplemented()
            }
        }
    }

    private fun createVerifyFaceControl(){
        mVerifyFaceControl.createCameraSource()
        mVerifyFaceControl.setVerifyFaceListener(object : VerifyFaceControl.OnVerifyFaceListener{
            override fun onResult(boolean: Boolean, faceByteArray: ByteArray?, face: FirebaseVisionFace?) {
                val faceData = HashMap<String,Any?>()
                faceData["faceData"] = faceByteArray
                faceData["faceTop"] = face!!.boundingBox.top.toDouble()
                faceData["faceLeft"] = face.boundingBox.left.toDouble()
                faceData["faceRight"] = face.boundingBox.right.toDouble()
                faceData["faceBottom"] = face.boundingBox.bottom.toDouble()
                faceData["faceHeadEulerAngleY"] = face.headEulerAngleY
                faceData["faceHeadEulerAngleZ"] = face.headEulerAngleZ
                faceData["faceTrackingId"] = face.trackingId
                mEventSink?.success(faceData)
            }

            override fun onDetecting(boolean: Boolean) {

            }


        })
        Log.e("FlutterWeeCameraMlkit","CreateVerifyFaceControl")

    }

    private fun checkPermission(){
        val mPermissionListener = object : PermissionListener {
            override fun onPermissionGranted() {
                createVerifyFaceControl()
                mVerifyFaceControl.startAction()
                mResult?.success(true)
            }
            override fun onPermissionDenied(deniedPermissions: ArrayList<String>?) {
                if(deniedPermissions!=null){
                    if(!deniedPermissions.contains(Manifest.permission.CAMERA)){
                        createVerifyFaceControl()
                        mVerifyFaceControl.startAction()
                        mResult?.success(true)
                    }
                }

            }
        }
        TedPermission.with(mContext)
                .setPermissionListener(mPermissionListener)
                .setPermissions(Manifest.permission.CAMERA)
                .check()
    }

}