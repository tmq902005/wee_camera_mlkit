package wee.flutter.wee_camera_mlkit;

import android.content.Context;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class WeeCameraMlkitFactory extends PlatformViewFactory {
    private final BinaryMessenger messenger;

    public WeeCameraMlkitFactory(BinaryMessenger messenger) {
        super(StandardMessageCodec.INSTANCE);
        this.messenger = messenger;
    }

    @Override
    public PlatformView create(Context context, int i, Object o) {
        return new FlutterWeeCameraMlkit(context,messenger,i);
    }
}
