package wee.flutter.wee_camera_mlkit;

/**
 * Created by Ezequiel Adrian on 24/02/2017.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MergeCursor;
import android.graphics.Point;
import android.graphics.PointF;
import android.media.MediaMetadataRetriever;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Utils {
    public static int widthScreen=0;
    public static int heightScreen=0;
    private static int widthDefault=dpToPx(667);
    private static int heightDefault=dpToPx(375);

    public static Float getScreenWidthFloat(Context c){
        return (float) getScreenWidth(c);
    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static Float dpToPxFloat(int dp){
        return dp * Resources.getSystem().getDisplayMetrics().density;
    }

    public static float pxToDp(final float px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDpInt(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }


    public static int getScreenHeight(Context c) {
        if(heightScreen==0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            heightScreen=size.y;
        }
        return heightScreen;
    }

    public static int getScreenWidth(Context c) {
        if(widthScreen==0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            widthScreen=size.x;
        }
        //---
        return widthScreen;
    }

    public static int reCalculatorWidth(Context context, int width){
        if(widthScreen==0){
            widthScreen=getScreenWidth(context);
        }
        return width*widthScreen/widthDefault;
    }

    public static int reCalculatorHeight(Context context, int height){
        if(heightScreen==0){
            heightScreen=getScreenHeight(context);
        }
        return height*heightScreen/heightDefault;
    }

    public static float getScreenRatio(Context c) {
        DisplayMetrics metrics = c.getResources().getDisplayMetrics();
        return ((float)metrics.heightPixels / (float)metrics.widthPixels);
    }

    public static int getScreenRotation(Context c) {
        WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
        return wm.getDefaultDisplay().getRotation();
    }

    public static int distancePointsF(PointF p1, PointF p2) {
        return (int) Math.sqrt((p1.x - p2.x) *  (p1.x - p2.x) + (p1.y - p2.y) *  (p1.y - p2.y));
    }

    public static PointF middlePoint(PointF p1, PointF p2) {
        if(p1 == null || p2 == null)
            return null;
        return new PointF((p1.x+p2.x)/2, (p1.y+p2.y)/2);
    }

    public static int inverse( int degrees ) { return 360 - (degrees % 360); }

    public static byte[] readFileResource(Class<?> resourceClass, String resourcePath) throws IOException {
        InputStream is = resourceClass.getClassLoader().getResourceAsStream(resourcePath);
        if (is == null)
            throw new IOException("cannot find resource: "+resourcePath);
        return getBytesFromInputStream(is);
    }

    public static byte[] getBytesFromInputStream(InputStream is) throws IOException {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        for (int len; (len = is.read(buffer)) != -1;) {
            os.write(buffer, 0, len);
        }
        os.flush();
        return os.toByteArray();
    }

    public static void allertMessage(Context context, String message) {
        new AlertDialog.Builder(context).setMessage(message).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    public static void toastMessage(final Activity activity, final String message) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        });
    }
    public static Date stringToDate(String string)throws Exception {

        Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(string);

        return date1;
    }
    public static String stringToDate(Date date)throws Exception {

        String date2=new SimpleDateFormat("dd/MM/yyyy").format(date);

        return date2;
    }

    public static String convertEmoji(String content) {
        content = content.replaceAll("U\\+", "0x");
        String keyword = "0x";

        int index = content.indexOf(keyword);
        int spaceIndex;

        while (index >=0){
            spaceIndex = content.indexOf(" ", index);

            if(spaceIndex > index) {
                String emoji = content.substring(index, spaceIndex);
                content = content.replaceAll(emoji, getEmoticon(Integer.decode(emoji)));
            }
            index = content.indexOf(keyword, index+keyword.length());
        }

        return content;
    }
    public static String getEmoticon(int originalUnicode) {
        return new String(Character.toChars(originalUnicode));
    }

    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean activeNetwork (Activity activity) {
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();

        return isConnected;

    }


    public static byte[] convertVideoAudio(String path) throws IOException {

        FileInputStream fis = new FileInputStream(path);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] b = new byte[1024];

        for (int readNum; (readNum = fis.read(b)) != -1;) {
            bos.write(b, 0, readNum);
        }

        byte[] bytes = bos.toByteArray();

        return bytes;
    }

    public static Long getDuration (String path, Context context) {
        Uri uri = Uri.parse(path);
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(context,uri);
        String durationStr = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
        return Long.parseLong(durationStr);
    }

    public static List<String> getAllMediaFilesOnDevice(Context context) {
        List<String> files = new ArrayList<>();
        try {

            final String[] columns = { MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DATE_ADDED,
                    MediaStore.Images.Media.BUCKET_ID,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

            MergeCursor cursor = new MergeCursor(new Cursor[]{context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, columns, null, null, null),
                    context.getContentResolver().query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, columns, null, null, null),
                    context.getContentResolver().query(MediaStore.Images.Media.INTERNAL_CONTENT_URI, columns, null, null, null),
                    context.getContentResolver().query(MediaStore.Video.Media.INTERNAL_CONTENT_URI, columns, null, null, null)
            });
            cursor.moveToFirst();
            files.clear();
            while (!cursor.isAfterLast()){
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                int lastPoint = path.lastIndexOf(".");
                path = path.substring(0, lastPoint) + path.substring(lastPoint).toLowerCase();
                files.add(path);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return files;
    }
}
