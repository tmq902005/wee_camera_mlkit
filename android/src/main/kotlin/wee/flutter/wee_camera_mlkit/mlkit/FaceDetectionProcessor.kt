package wee.flutter.wee_camera_mlkit.mlkit

import android.graphics.Bitmap
import android.util.Log
import co.metalab.asyncawait.async
import com.google.android.gms.tasks.Task
import com.google.firebase.ml.vision.FirebaseVision
import com.google.firebase.ml.vision.common.FirebaseVisionImage
import com.google.firebase.ml.vision.face.FirebaseVisionFace
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

/** Face Detector Demo.  */
class FaceDetectionProcessor : VisionProcessorBase<List<FirebaseVisionFace>>() {

    private val faceDetector: FirebaseVisionFaceDetector
    private var fireRawImage: FirebaseVisionImage? = null
    private var verifyFaceListener: VerifyFaceListener? = null
    private var faces: List<FirebaseVisionFace>? = null
    private var isGetLandMarks: Boolean? = false
    private var isProcessing = false

    val faceResultRaw: FaceResult
        get() {
            return try {
                val bitmap = fireRawImage!!.bitmapForDebugging
                isGetLandMarks = false
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream)
                val byteArray = byteArrayOutputStream.toByteArray()
                bitmap.recycle()
                FaceResult(getLargestFace(faces!!), byteArray)
            } catch (ex: Exception) {
                FaceResult(null, null)
            }

        }

    interface VerifyFaceListener {
        fun onUpdateFace(largestFace: FaceResult?)
        fun onTrackingFace(face: FirebaseVisionFace?)
    }

    fun setOnVerifyFaceListener(listener: VerifyFaceListener) {
        verifyFaceListener = listener
    }

    init {
        val options = FirebaseVisionFaceDetectorOptions.Builder()
                .setClassificationMode(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                .setPerformanceMode(FirebaseVisionFaceDetectorOptions.ACCURATE)
                .setLandmarkMode(FirebaseVisionFaceDetectorOptions.ALL_LANDMARKS)
                .setContourMode(FirebaseVisionFaceDetectorOptions.NO_CONTOURS)
                .enableTracking()
                .setMinFaceSize(0.5f)
                .build()

        faceDetector = FirebaseVision.getInstance().getVisionFaceDetector(options)
    }

    override fun stop() {
        try {
            faceDetector.close()
            //labelDetector.stop();
        } catch (e: IOException) {
            Log.e(TAG, "Exception thrown while trying to close Face Detector: $e")
        }

    }

    override fun detectInImage(image: FirebaseVisionImage): Task<List<FirebaseVisionFace>> {
        fireRawImage = image
        return faceDetector.detectInImage(fireRawImage!!)
    }

    override fun onSuccess(
            faces: List<FirebaseVisionFace>,
            frameMetadata: FrameMetadata,
            graphicOverlay: GraphicOverlay) {

        //---
        this.faces = faces
        if (verifyFaceListener != null && !isProcessing) {
            isProcessing = true
            val largestFace = getLargestFace(faces)
            verifyFaceListener!!.onTrackingFace(largestFace)
            isProcessing = false
            /*if (largestFace != null) {
                async {
                    val result = await {
                        faceResultRaw
                    }
                    result.face = largestFace
                    verifyFaceListener!!.onUpdateFace(result)
                    isProcessing = false
                }
            }else{
                isProcessing = false
            }*/

        }

    }

    private fun checkLandmark(largestFace: FirebaseVisionFace): Boolean {
        val eyeLeft = largestFace.leftEyeOpenProbability
        val eyeRight = largestFace.rightEyeOpenProbability
        val headEulerAngleY = largestFace.headEulerAngleY
        val headEulerAngleZ = largestFace.headEulerAngleZ
        /*val nose = largestFace.getLandmark(FirebaseVisionFaceLandmark.NOSE_BASE)
        val lCheek = largestFace.getLandmark(FirebaseVisionFaceLandmark.LEFT_CHEEK)
        val rCheek = largestFace.getLandmark(FirebaseVisionFaceLandmark.RIGHT_CHEEK)
        val lMouth = largestFace.getLandmark(FirebaseVisionFaceLandmark.MOUTH_LEFT)
        val rMouth = largestFace.getLandmark(FirebaseVisionFaceLandmark.MOUTH_RIGHT)
        val bMouth = largestFace.getLandmark(FirebaseVisionFaceLandmark.MOUTH_BOTTOM)*/
        return headEulerAngleY in -7f..7f && headEulerAngleZ in -7f..7f &&
                eyeLeft>0.4 && eyeRight>0.4 /*&& nose!=null && lMouth!=null && rMouth!=null && bMouth!=null
                && lCheek!=null && rCheek!=null*/
    }

    override fun onFailure(e: Exception) {
        Log.e(TAG, "Face detection failed $e")
    }

    //---
    private fun getLargestFace(faces: List<FirebaseVisionFace>): FirebaseVisionFace? {
        var largestFace: FirebaseVisionFace? = null
        val mList = ArrayList<String>()
        if (faces.isNotEmpty()) {
            for (i in faces.indices) {
                mList.add("" + faces[i].trackingId)
                val face = faces[i]
                val faceId = "" + face.trackingId
                if (largestFace == null || getFaceSize(largestFace) < getFaceSize(face)) {
                    largestFace = face
                }
            }
        }
        //Log.e("ListIDFaceActive",""+PubKt.getListActiveFaceId());
        return largestFace
    }


    private fun getFaceSize(face: FirebaseVisionFace): Int {
        val bounds = face.boundingBox
        return bounds.height() * bounds.width()
    }

    companion object {
        private val TAG = "FaceDetectionProcessor"
        fun scaleDown(realImage: Bitmap, maxImageSize: Float,
                      filter: Boolean): Bitmap {
            val ratio = Math.min(
                    maxImageSize / realImage.width,
                    maxImageSize / realImage.height)
            val width = Math.round(ratio * realImage.width)
            val height = Math.round(ratio * realImage.height)

            return Bitmap.createScaledBitmap(realImage, width,
                    height, filter)
        }
    }

}

class FaceResult(var face: FirebaseVisionFace?, var faceData:ByteArray?)