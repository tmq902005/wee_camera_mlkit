import 'package:flutter/material.dart';
import 'dart:async';

import 'package:wee_camera_mlkit/wee_camera_mlkit.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  WeeCameraMlkitControler weeCameraMlkitControler;
  @override
  void initState() {
    super.initState();

  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<dynamic> initPlatformState() async {
    bool startCamera;

    // Platform messages may fail, so we use a try/catch PlatformException.
    startCamera = await weeCameraMlkitControler.startCamera;
    if(startCamera) {
      /*weeCameraMlkitControler.startFaceStream((data) {
        print(data);
      });*/
    }


    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: WeeCameraMlkit(
            onCameraMlkitCreated: _onCameraCreated,
          ),
        ),
      ),
    );
  }
}

void _onCameraCreated(WeeCameraMlkitControler controller) async{
  bool startCamera;
  int timeIn = DateTime.now().millisecondsSinceEpoch;
  int timeOut = 0;
  // Platform messages may fail, so we use a try/catch PlatformException.
  startCamera = await controller.startCamera;
  if(startCamera) {
    controller.startFaceStream((faceData) {
      timeOut = DateTime.now().millisecondsSinceEpoch;
      print("FaceID: ${faceData.faceTrackingId}"+ "[ ${timeOut-timeIn} ]");
      timeIn = DateTime.now().millisecondsSinceEpoch;
    });
  }

}