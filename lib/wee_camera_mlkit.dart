import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

typedef onLatestFaceAvailable = Function(FaceData image);
typedef void WeeCameraMlkitCreatedCallback(WeeCameraMlkitControler controller);

class WeeCameraMlkit extends StatefulWidget {
  final WeeCameraMlkitCreatedCallback onCameraMlkitCreated;

  const WeeCameraMlkit({
    Key key,
    this.onCameraMlkitCreated
  }):super(key:key);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WeeCameraMlkit();
  }

}

class _WeeCameraMlkit extends State<WeeCameraMlkit>{
  @override
  Widget build(BuildContext context) {
    if (defaultTargetPlatform == TargetPlatform.android) {
      return AndroidView(
        viewType: 'wee.flutter.weecameramlkit/camera',
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    return Text(
        '$defaultTargetPlatform is not yet supported by the text_view plugin');
  }
  void _onPlatformViewCreated(int id) {
    if (widget.onCameraMlkitCreated == null) {
      return;
    }
    widget.onCameraMlkitCreated(new WeeCameraMlkitControler._(id));
  }

}

class WeeCameraMlkitControler {

  StreamSubscription<dynamic> _imageStreamSubscription;
  final MethodChannel _channel;

  WeeCameraMlkitControler._(int id): _channel = new MethodChannel('wee.flutter.weecameramlkit/camera_$id');

  Future<bool> get checkPermission async {
    return  await _channel.invokeMethod('checkPermission');
  }

  Future<bool> get startCamera async {
    return  await _channel.invokeMethod('startCamera');
  }

  Future<void> startFaceStream(onLatestFaceAvailable onAvailable) async {

    try {
      await _channel.invokeMethod('startFaceStream');
    } on PlatformException catch (e) {
      print(e.toString());
    }
    const EventChannel cameraEventChannel =
    EventChannel("wee_camera_mlkit/stream");
    _imageStreamSubscription =
        cameraEventChannel.receiveBroadcastStream().listen((dynamic imageData) {
          var data = FaceData._fromPlatformData(imageData);

          if(data.faceData!=null){
            onAvailable(data);
            print(data.faceRect);
          }

        },
        );
  }

  Future<void> stopFaceStream() async{
    try{
      await _channel.invokeMethod('stopFaceStream');
    }on PlatformException catch (e){
      print(e.toString());
    }
    _imageStreamSubscription.cancel();
    _imageStreamSubscription=null;
  }
}



class FaceData {
  Uint8List faceData;
  Rect faceRect;
  double faceHeadEulerAngleY;
  double faceHeadEulerAngleZ;
  int faceTrackingId;
  FaceData._fromPlatformData(Map<dynamic,dynamic> data){
    faceData = data["faceData"];
    faceRect = Rect.fromLTRB(data["faceLeft"], data["faceTop"], data["faceRight"], data["faceBottom"]);
    faceHeadEulerAngleY = data["faceHeadEulerAngleY"];
    faceHeadEulerAngleZ = data["faceHeadEulerAngleZ"];
    faceTrackingId = data["faceTrackingId"];
  }
}
  /*FaceData._fromPlatformData(Map<dynamic, dynamic> data){
      : faceData;
  *//*planes = List<Plane>.unmodifiable(data['planes']
            .map((dynamic planeData) => Plane._fromPlatformData(planeData)));*//*
  final Uint8List faceData;
*//* final List<Plane> planes;*//*
}*/
